#!/bin/bash
curr=0;
prev=0;

while true; do 
    curr=`wget --quiet http://www.brillig.com/debt_clock -O - | \
grep debtiv.gif | \
sed -e 's/.*ALT="//' -e 's/".*//' -e 's/\ //g' -e 's/^\\$//'`;
    echo -n `date +%d/%m/%Y-%H:%M:%S`" "
    echo -n "$"$curr " "
    curr=`echo "$curr" | sed -e 's/,//g' -e 's/\..*$//g'`
    if [ $prev != 0 -a $curr != 0 ]; then 
       echo -n "Grown by: $"; 
       expr $curr - $prev; 
    else 
       echo
    fi
    sleep 1;
    prev=$curr
done
